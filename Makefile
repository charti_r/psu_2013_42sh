##
## Makefile for 42sh in /home/charti_r/rendu/PSU_2013_42sh
## 
## Made by charti
## Login   <charti_r@epitech.net>
## 
## Started on  Sat Mar 15 12:02:02 2014 charti
## Last update Fri May 30 14:38:24 2014 charti
##

CFLAGS		+= -W -Wall -pedantic -g3
CFLAGS		+= -I./header/ -I./libmy/header/

CC		= gcc

RM		= rm -f

NAME		= 42sh

LIBMY		= libmy/
LIB		= -lmy -L ./$(LIBMY)

SRC_DIR		= source/
CMD_DIR		= $(SRC_DIR)command/
ENV_DIR		= $(SRC_DIR)env/

SRC		=  $(SRC_DIR)main.c \
		   $(SRC_DIR)prompt.c \
		   $(SRC_DIR)read_line.c \
		   $(SRC_DIR)exec_cmd.c \
		   $(SRC_DIR)exec_prog.c \
		   $(SRC_DIR)redir.c \
		   $(SRC_DIR)clean_argv.c \
		   $(SRC_DIR)parse.c \
		   $(SRC_DIR)sig_catch.c \
		   $(SRC_DIR)child.c \
		   $(SRC_DIR)parent.c \
		   $(SRC_DIR)ctab.c

##
## Cmd.
##
SRC		+= $(CMD_DIR)my_own_cmd.c \
		   $(CMD_DIR)exit_cmd.c \
		   $(CMD_DIR)cd_cmd.c \
		   $(CMD_DIR)cd_pwd_change.c \
		   $(CMD_DIR)env_cmd.c \
		   $(CMD_DIR)setenv_cmd.c \
		   $(CMD_DIR)unsetenv_cmd.c \
		   $(CMD_DIR)echo_cmd.c

##
## Env.
##
SRC		+= $(ENV_DIR)list.c \
		   $(ENV_DIR)cp_env.c \
		   $(ENV_DIR)getenv.c \
		   $(ENV_DIR)setenv.c \
		   $(ENV_DIR)unsetenv.c

OBJ		= $(SRC:.c=.o)

all: makelib $(NAME)

makelib:
	make -C $(LIBMY)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(LIB) -o $(NAME)

clean:
	make clean -C $(LIBMY)
	$(RM) $(OBJ)

fclean: clean
	make fclean -C $(LIBMY)
	$(RM) $(NAME)

re: fclean all

.PHONY: all makelib clean fclean re
