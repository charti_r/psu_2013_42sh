/*
** mysh.h for 42sh in /home/charti_r/rendu/PSU_2013_42sh/header
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Mar 15 12:03:04 2014 charti
** Last update Fri May 30 14:38:30 2014 charti
*/

#ifndef MYSH_H_
# define MYSH_H_

/*
** Std Include.
*/
# include <unistd.h>
# include <sys/wait.h>
# include <signal.h>

/*
** My Include.
*/
# include "my.h"
# include "constante.h"
# include "structure.h"

/*
** Prompt.
*/
void	prompt(t_list *env, int add);

/*
** Exec.
*/
int	exec_cmd(char *str, t_list *env);
int	exec_prog(char *str, t_list *env);
int	redir(char **cmd, int *fd);
char	**clean_argv(char **argv);

/*
** Parse.
*/
int	parse_replace(t_list *env, char **argv);

/*
** Catch Sig.
*/
void	sigint_catch(int sig, t_list *ptr_env);

/*
** Cmd.
*/
int	in_own_cmd(int argc, char **argv, t_list *env);
int	out_own_cmd(char **argv, t_list *env);
void	exit_shell(char **argv);
int	cd_cmd(char **argv, t_list *env);
int	cd_pwd_change(char *str, t_elem *pwd, t_elem *oldpwd);
int	env_cmd(t_list *env);
int	setenv_cmd(int argc, char **argv, t_list *env);
int	unsetenv_cmd(char **argv, t_list *env);

/*
** Child
*/
int	child(char **argv, t_list *env);

/*
** Parent.
*/
int	parent(void);

/*
** Ctab.
*/
void	my_aff_ctab(char **tab);
int	my_count_double_ctab(char **tab);
void	my_free_double_ctab(char **tab);

/*
** Env.
*/
int	cp_env(t_list *list, char **env);
char	*get_env_line(char *var, t_list *env);
int	set_env_line(char *var, char *value, t_list *env);
int	del_env_line(char *var, t_list *env);

/*
** List.
*/
void	init_list(t_list *list);
int	add_elem(t_list *list, char *var, char *value);
int	del_elem(t_list *list, char *var);
char	**env_from_list(t_list *list);
t_elem	*get_env_elem(t_list *list, char *var);

/*
** Read.
*/
char	*read_line(void);

/*
** Echo.
*/
int	echo_cmd(char **argv);

#endif /* !MYSH_H_ */
