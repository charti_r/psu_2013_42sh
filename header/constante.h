/*
** constante.h for mysh in /home/charti_r/rendu/PSU_2013_minishell2/header
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Feb 11 11:28:51 2014 charti
** Last update Sun May 25 19:46:45 2014 charti
*/

#ifndef CONSTANTE_H_
# define CONSTANTE_H_

/*
** Open Const
*/
# define RD_FILE	O_RDONLY
# define WR_FILE	O_WRONLY | O_CREAT
# define MODE		S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH

/*
** Define From Main
*/
# define DEFAULT_PROMPT	"$> "
# define SIG_QUIT	"Quit from keyboard\n"
# define SIG_ILLEGAL	"Illegal Instruction\n"
# define SIG_ABORT	"Abort signal from abort\n"
# define SIG_FLOAT	"Floating point exception\n"
# define SIG_SEGV	"Invalid memory reference\n"
# define CMD_NOT_FOUND	"%s: command not found\n"
# define USER_EXIT	"exit\n"
# define FAILED_FORK	"fork failed\n"
# define SYNTAX_ERROR	"Syntax Error\n"

/*
** Define from Environement files
*/
# define VAR_NOT_FOUND	"%s: not a variable\n"

/*
** Commande Syntax
*/
# define CMD_SEP	";"
# define CMD_AND_SEP	"&&"
# define CMD_OR_SEP	"||"
# define ARG_SEP	" "
# define PIPE_SEP	"|"

/*
** Buildins Cmd
*/
# define CMD_CD		"cd"
# define CMD_SETENV	"setenv"
# define CMD_UNSETENV	"unsetenv"
# define CMD_ENV	"env"
# define CMD_EXIT	"exit"
# define CMD_ECHO	"echo"

/*
** Pipe define
*/
# define FST		1
# define LST		2
# define PIPE(i, cmd)	(i ? (cmd ? (FST | LST) : LST) : (cmd ? FST : 0))

/*
** Define from Builtins files
*/
# define UNSETENV_FAILED	"bad usage: unsetenv [variable]\n"
# define SETENV_FAILED		"bad usage: setenv [variable] (value)\n"
# define CD_DENIED		"mysh: %s: Permission denied\n"
# define CD_NOT_FOUND		"mysh: %s: No such file or directory\n"

#endif /* !CONSTANTE_H_ */
