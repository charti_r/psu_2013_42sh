/*
** structure.h for 42sh in /home/charti_r/rendu/PSU_2013_42sh/header
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Mar 15 12:07:21 2014 charti
** Last update Fri May 23 10:19:06 2014 charti
*/

#ifndef STRUCTURE_H_
# define STRUCTURE_H_

typedef struct	s_elem
{
  char		*var;
  char		*value;
  struct s_elem	*next;
}		t_elem;

typedef struct	s_list
{
  int		nb;
  t_elem	*first;
}		t_list;

typedef struct	s_pipe
{
  int		p[2];
  int		fd_s;
}		t_pipe;

#endif /* !STRUCTURE_H_ */
