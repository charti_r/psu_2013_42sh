/*
** my_constante.h for libmy in /home/charti_r/Libraries/libmy/header
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 17:33:35 2014 charti
** Last update Mon Mar 17 15:11:48 2014 charti
*/

#ifndef MY_CONSTANTE_H_
# define MY_CONSTANTE_H_

/*
** Macros
*/
# define MY_MAX(a, b) ((a) > (b) ? (a) : (b))
# define MY_MIN(a, b) ((a) < (b) ? (a) : (b))
# define MY_ISNEG(a) ((a) < 0 ? 1 : 0)
# define MY_ISPOS(a) ((a) > 0 ? 1 : 0)
# define MY_ABS(a) ((a) > 0 ? (a) : (-a))

/*
** Const
*/
# define MY_BUFF_SIZE 8000

#endif /* !MY_CONSTANTE_H_ */
