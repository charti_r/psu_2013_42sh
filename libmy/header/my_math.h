/*
** my_math.h for libmy in /home/charti_r/Libraries/libmy/header
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 15:51:29 2014 charti
** Last update Wed Feb 26 17:09:48 2014 charti
*/

#ifndef MY_MATH_H_
# define MY_MATH_H_

# include "my.h"

int	my_nbr_isneg(char *str);
int	my_pow(int nb, int pow);
int	my_sqrt(int nb);
int	my_find_primeup(int nb);
int	my_is_prime(int nb);

#endif /* !MY_MATH_H_ */
