/*
** my_string.h for libmy in /home/charti_r/Libraries/libmy/header
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 15:36:27 2014 charti
** Last update Thu May 22 10:23:43 2014 charti
*/

#ifndef MY_STRING_H_
# define MY_STRING_H_

# include "my.h"

/*
** Manipulate
*/
char	*my_revstr(char *str);
char	*my_strcat(char *dest, char *src);
char	*my_strcpy(char *dest, char *src);
char	*my_strcpy_alloc(char *dest, char *src);
char	*my_strlcat(char *dest, char *src, int l);
char	*my_strlcpy(char *dest, char *src, int l);
char	*my_strncat(char *dest, char *src, int n);
char	*my_strncpy(char *dest, char *src, int n);

/*
** Compare
*/
int	my_charcmp(char c, char *str);
int	my_strcmp(char *s1, char *s2);
int	my_strncmp(char *s1, char *s2, int n);
int	my_strstr(char *str, char *to_find);
int	my_str_isalpha(char *str);
int	my_str_isnum(char *str);
int	my_str_islower(char *str);
int	my_str_isupper(char *str);
int	my_str_isprintable(char *str);

/*
** Modify Display
*/
char	*my_strcapitalize(char *str);
char	*my_strlowcase(char *str);
char	*my_strupcase(char *str);
int	my_showstr(char *str);

#endif /* !MY_STRING_H_ */
