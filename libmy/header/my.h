/*
** my.h for my.h in /home/charti_r/rendu/Piscine-C-include
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu Oct 10 18:35:28 2013 charti
** Last update Sun May 25 12:47:00 2014 chauvi_r
*/

#ifndef MY_H_
# define MY_H_

/*
** Std Include
*/
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <sys/stat.h>
# include <fcntl.h>

/*
** My Include
*/
# include "my_constante.h"
# include "my_string.h"
# include "my_math.h"

/*
** Out Stream
*/
int	my_fprintf(int fd, const char *str, ...);
int	my_fputchar(int fd, char c);
int	my_fputstr(int fd, char *str);
void	my_fputnbr(int fd, int nb);
void	my_fputnbr_base(int fd, int nb, char *base);

int	my_printf(const char *str, ...);
int	my_putchar(char c);
int	my_putstr(char *str);
void	my_putnbr(int nb);
void	my_putnbr_base(int nb, char *base);

/*
** In Stream
*/
char	*get_next_line(int fd);

/*
** Count the lenght of the string
*/
int	my_strlen(char *str);
int	my_jump_str(char *str, char *to_jump);
int	my_jump_char(char *str, char c);
int	my_jump_space(char *str);
int	my_sizeof_char(char *str, char c);
int	my_sizeof_line(char *str);
int	my_sizeof_nbrbase(int nb, int base);
int	my_sizeof_nbr(int nb);

/*
** Change or orgenize values
*/
void	my_swap_int(int *a, int *b);
void	my_swap_char(char *a, char *b);
void	my_sort_int_tab(int *tab, int size);
char	**my_str_to_wordtab(char *str, char *sep);
char	*my_str_realloc(char *str, int size);
char	*my_clear_str(char *str);
int	my_replace_char(char *str, char char1, char char2);

/*
** Maths
*/
int	my_getnbr(char *str);
int	my_getnbr_base(char *str, char *base);

#endif /* !MY_H_ */
