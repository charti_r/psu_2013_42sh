/*
** my_sizeof_char.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed May 14 17:50:15 2014 charti
** Last update Thu May 15 10:05:16 2014 charti
*/

#include "my.h"

int	my_sizeof_char(char *str, char c)
{
  int	i;

  i = 0;
  while (str[i] && str[i] != c)
    ++i;
  return (i);
}
