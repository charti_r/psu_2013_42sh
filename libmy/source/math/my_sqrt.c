/*
** my_sqrt.c for libmy in /home/charti_r/Libraries/libmy/source/math
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 17:09:53 2014 charti
** Last update Wed Feb 26 17:09:54 2014 charti
*/

#include "my.h"

int	my_sqrt(int nb)
{
  int	sqrt;

  sqrt = 0;
  while ((sqrt * sqrt) < nb)
    ++sqrt;
  if ((sqrt * sqrt) == nb)
    return (sqrt);
  else
    return (0);
}
