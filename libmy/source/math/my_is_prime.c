/*
** my_is_prime.c for libmy in /home/charti_r/Libraries/libmy/source/math
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 17:05:54 2014 charti
** Last update Wed Feb 26 17:05:55 2014 charti
*/

#include "my.h"

int	my_is_prime(int nb)
{
  int	i;

  if (nb < 2)
    return (0);
  i = 2;
  while (i < nb)
    {
      if (!(nb % i))
	return (0);
     ++i;
    }
  return (1);
}
