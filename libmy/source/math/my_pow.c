/*
** my_pow.c for libmy in /home/charti_r/Libraries/libmy/source/math
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 17:06:38 2014 charti
** Last update Wed Feb 26 17:07:23 2014 charti
*/

#include "my.h"

int	my_pow(int nb, int pow)
{
  if (pow < 0)
    return (0);
  if (!pow)
    return (1);
  if (pow == 1)
    return (nb);
  return (nb * my_pow(nb, pow - 1));
}
