/*
** my_jump_char.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu May 15 09:04:58 2014 charti
** Last update Thu May 22 09:06:15 2014 charti
*/

#include "my.h"

int	my_jump_char(char *str, char c)
{
  int	i;

  i = my_sizeof_char(str, c);
  while (str[i] == c)
    ++i;
  return (i);
}
