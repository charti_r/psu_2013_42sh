/*
** my_clear_str.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Feb 25 12:26:37 2014 charti
** Last update Tue Feb 25 12:29:03 2014 charti
*/

#include "my.h"

char	*my_clear_str(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      str[i] = 0;
      ++i;
    }
  return (str);
}
