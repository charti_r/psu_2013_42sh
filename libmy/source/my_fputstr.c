/*
** my_fputstr.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Jan 22 20:24:55 2014 charti
** Last update Wed Jan 22 20:25:51 2014 charti
*/

#include "my.h"

int	my_fputstr(int fd, char *str)
{
  return (write(fd, str, my_strlen(str)));
}
