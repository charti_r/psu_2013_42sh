/*
** my_printf.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 12:23:12 2013 charti
** Last update Tue Jan 21 14:53:45 2014 charti
*/

#include "my.h"

void	my_printf_flag(char flag, va_list ptr_list);

int		my_printf(const char *str, ...)
{
  va_list	ptr_list;
  int		i;

  va_start(ptr_list, str);
  i = 0;
  while (str[i])
    {
      if (str[i] == '%')
	{
	  ++i;
	  my_printf_flag(str[i], ptr_list);
	}
      else
	my_putchar(str[i]);
      ++i;
    }
  va_end(ptr_list);
  return (i);
}

void	my_printf_flag(char flag, va_list ptr_list)
{
  if (flag == 'd' || flag == 'i')
    my_putnbr(va_arg(ptr_list, int));
  if (flag == 'u')
    my_putnbr(va_arg(ptr_list, unsigned int));
  if (flag == 'b')
    my_putnbr_base(va_arg(ptr_list, int), "01");
  if (flag == 'o')
    my_putnbr_base(va_arg(ptr_list, int), "01234567");
  if (flag == 'p' || flag == 'P')
    my_putstr("0x");
  if (flag == 'x' || flag == 'p')
    my_putnbr_base(va_arg(ptr_list, unsigned int), "0123456789abcdef");
  if (flag == 'X' || flag == 'P')
    my_putnbr_base(va_arg(ptr_list, unsigned int), "0123456789ABCDEF");
  if (flag == 'c')
    my_putchar(va_arg(ptr_list, int));
  if (flag == 's')
    my_putstr(va_arg(ptr_list, char*));
  if (flag == '%')
    my_putchar('%');
}
