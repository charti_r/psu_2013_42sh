/*
** my_str_realloc.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 13:00:57 2013 charti
** Last update Wed May 14 11:24:20 2014 abboud_j
*/

#include "my.h"

char	*my_str_realloc(char *str, int size)
{
  char	*new_str;

  if ((new_str = malloc(sizeof(*new_str) * (size + 1))) == NULL)
    return (NULL);
  if (str)
    return (my_strcpy(new_str, str));
  my_clear_str(new_str);
  return (new_str);
}
