/*
** my_putnbr_base.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 12:25:39 2013 charti
** Last update Mon Dec 30 12:25:44 2013 charti
*/

#include "my.h"

void     my_putnbr_base(int nb, char *base)
{
  int   n_base;

  if (nb < 0)
    {
      my_putchar('-');
      nb = -nb;
    }
  n_base = my_strlen(base);
  if (nb >= n_base)
    {
      my_putnbr_base(nb / n_base, base);
      my_putnbr_base(nb % n_base, base);
    }
  else
    my_putchar(base[nb]);
}
