/*
** my_jump_str.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu May 22 09:05:23 2014 charti
** Last update Thu May 22 09:08:42 2014 charti
*/

#include "my.h"

int	my_jump_str(char *str, char *to_jump)
{
  int	i;

  if ((i = my_strstr(str, to_jump)) == -1)
    return (my_strlen(str));
  while (my_strstr(&str[i], to_jump) == 0)
    i += my_strlen(to_jump);
  return (i);
}
