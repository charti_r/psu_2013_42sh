/*
** my_strlen.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 12:58:05 2013 charti
** Last update Tue Jan 21 16:33:06 2014 charti
*/

#include "my.h"

int	my_strlen(char *str)
{
  int	i;

  if (!str)
    return (0);
  i = 0;
  while (str[i])
    ++i;
  return (i);
}
