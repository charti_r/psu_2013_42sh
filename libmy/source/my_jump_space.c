/*
** my_jump_space.c for libmy in /home/charti_r/Librarieslibmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri May 16 10:37:55 2014 charti
** Last update Fri May 16 10:38:25 2014 charti
*/

#include "my.h"

int	my_jump_space(char *str)
{
  int		i;

  i = 0;
  while (str[i] == ' ' || str[i] == '\t')
    ++i;
  return (i);
}
