/*
** my_putchar.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 12:25:20 2013 charti
** Last update Wed Jan 22 20:28:29 2014 charti
*/

#include "my.h"

int	my_putchar(char c)
{
  return (write(1, &c, 1));
}
