/*
** my_fputnbr.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Feb 24 17:48:00 2014 charti
** Last update Sun May 25 13:29:46 2014 dantas_c
*/

#include "my.h"

void	my_fputnbr(int fd, int nb)
{
  if (nb < 0)
    {
      my_fputchar(fd, '-');
      nb = -nb;
    }
  if (nb >= 10)
    {
      my_fputnbr(fd, nb / 10);
      my_fputnbr(fd, nb % 10);
    }
  else
    my_fputchar(fd, nb + '0');
}
