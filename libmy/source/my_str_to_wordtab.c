/*
** my_str_to_wordtab.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed May 14 17:42:32 2014 charti
** Last update Thu May 22 10:22:00 2014 charti
*/

#include "my.h"

static int	count_word(char *str, char *sep)
{
  int		count;
  int		i;

  if (my_strstr(str, sep) == -1)
    return (1);
  count = 1;
  i = my_jump_space(str);
  while (str[i])
    {
      if (my_strstr(&str[i], sep) == 0)
	{
	  i += my_jump_str(&str[i], sep);
	  i += my_jump_space(&str[i]);
	  if (str[i])
	    ++count;
	}
      else
	++i;
    }
  return (count);
}

static char	*fill_word(char *str, char *sep)
{
  char		*word;
  int		size;

  size = ((size = my_strstr(str, sep)) == -1 ? my_strlen(str) : size);
  if ((word = malloc(sizeof(*word) * (size + 1))) == NULL)
    return (NULL);
  return (my_strncpy(word, str, size));
}

char	**my_str_to_wordtab(char *str, char *sep)
{
  char	**wordtab;
  int	size;
  int	count;
  int	i;

  size = count_word(str, sep);
  if ((wordtab = malloc(sizeof(*wordtab) * (size + 1))) == NULL)
    return (NULL);
  count  = 0;
  i = 0;
  while (count < size)
    {
      i += my_jump_space(&str[i]);
      if ((wordtab[count] = fill_word(&str[i], sep)) == NULL)
	return (NULL);
      i += my_jump_str(&str[i], sep);
      ++count;
    }
  wordtab[count] = NULL;
  return (wordtab);
}
