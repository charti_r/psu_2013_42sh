/*
** my_sort_int_tab.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 13:07:39 2013 charti
** Last update Tue Jan 21 16:15:15 2014 charti
*/

#include "my.h"

void	my_sort_int_tab(int *tab, int size)
{
  int	stop;
  int	i;

  stop = 0;
  while (!stop)
    {
      stop = 1;
      i = 0;
      while ((i + 1) < size)
	{
	  if (tab[i] > tab[i + 1])
	    {
	      my_swap_int(&tab[i], &tab[i + 1]);
	      stop = 0;
	    }
	  ++i;
	}
    }
}
