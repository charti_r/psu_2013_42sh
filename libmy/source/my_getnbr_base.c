/*
** my_getnbr_base.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 13:05:58 2013 charti
** Last update Tue Jan 21 14:39:41 2014 charti
*/

#include "my.h"

int	my_getnbr_base(char *str, char *base)
{
  int	i;
  int	j;
  int	nb;
  int	n_base;

  n_base = my_strlen(base);
  nb = 0;
  i = 0;
  while (str[i] == '+' || str[i] == '-')
    ++i;
  while (str[i])
    {
      j = 0;
      while (str[i] != base[j] && base[j])
	++j;
      if (j == n_base)
	return (nb * my_nbr_isneg(str));
      nb = nb * n_base;
      nb = nb + j;
      ++i;
    }
  return (nb * my_nbr_isneg(str));
}
