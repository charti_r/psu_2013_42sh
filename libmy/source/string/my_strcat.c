/*
** my_strcat.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:13:21 2014 charti
** Last update Wed Feb 26 16:13:22 2014 charti
*/

#include "my.h"

char	*my_strcat(char *dest, char *src)
{
  int	size;
  int	i;

  size = my_strlen(dest);
  i = 0;
  while (src[i])
    {
      dest[size + i] = src[i];
      ++i;
    }
  dest[size + i] = '\0';
  return (dest);
}
