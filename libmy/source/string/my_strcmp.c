/*
** my_strcmp.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:37:01 2014 charti
** Last update Mon Mar 17 23:54:19 2014 charti
*/

#include "my.h"

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] == s2[i] && (s1[i] && s2[i]))
    ++i;
  return (s1[i] - s2[i]);
}
