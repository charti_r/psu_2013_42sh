/*
** my_str_isalpha.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:15:07 2014 charti
** Last update Wed Feb 26 16:15:08 2014 charti
*/

#include "my.h"

int	my_str_isalpha(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] < 'a' || str[i] > 'z')
	if (str[i] < 'A' || str [i] > 'Z')
	  return (0);
      ++i;
    }
  return (1);
}
