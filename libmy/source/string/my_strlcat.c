/*
** my_strlcat.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:20:03 2014 charti
** Last update Wed Feb 26 16:20:04 2014 charti
*/

#include "my.h"

char	*my_strlcat(char *dest, char *src, int l)
{
  int   size;
  int   i;

  size = my_strlen(dest);
  i = 0;
  if (l > size)
    {
      while (src[i] && (i + size) < l)
	{
	  dest[size + i] = src[i];
	  ++i;
	}
    }
  dest[l] = '\0';
  return (dest);
}
