/*
** my_strstr.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:55:21 2014 charti
** Last update Thu May 22 08:49:46 2014 charti
*/

#include "my.h"

int	my_strstr(char *str, char *to_find)
{
  int	i;
  int	j;

  i = 0;
  while (str[i])
    {
      if (to_find[0] == str[i])
	{
	  j = 0;
	  while (to_find[j] == str[i + j] && (to_find[j] && str[i + j]))
	    ++j;
	  if (!to_find[j])
	    return (i);
	}
      ++i;
    }
  return (-1);
}
