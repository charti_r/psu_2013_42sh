/*
** my_replace_char.c for my_replace_char in /home/chauvi_r/Projets/psu_2013_42sh
** 
** Made by chauvi_r
** Login   <chauvi_r@work>
** 
** Started on  Sun May 25 12:31:54 2014 chauvi_r
** Last update Sun May 25 12:39:37 2014 chauvi_r
*/

#include "my.h"

int	my_replace_char(char *str, char char1, char char2)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] == char1)
	str[i] = char2;
      ++i;
    }
  return (0);
}
