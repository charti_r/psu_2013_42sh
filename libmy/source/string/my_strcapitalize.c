/*
** my_strcapitalize.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:11:58 2014 charti
** Last update Wed Feb 26 16:11:58 2014 charti
*/

#include "my.h"

char	*my_strcapitalize(char *str)
{
  int	i;

  if (str[0] >='a' && str[0] <= 'z')
    str[0] = str[0] - 32;
  i = 0;
  while (str[i])
    {
      if (str[i] == ' ')
	if (str[i + 1] >= 'a' && str[i + 1] <= 'z')
	  str[i + 1] = str[i + 1] - 32;
      if ((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z'))
	if (str[i + 1] >= 'A' && str[i + 1] <= 'Z')
	  str[i + 1] = str[i + 1] + 32;
      ++i;
    }
  return (str);
}
