/*
** my_strncmp.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:53:35 2014 charti
** Last update Wed Feb 26 16:54:24 2014 charti
*/

#include "my.h"

int     my_strncmp(char *s1, char *s2, int n)
{
  int   i;

  i = 0;
  while ((s1[i] == s2[i] && i < n) && (s1[i] && s2[i]))
    ++i;
  return (s1[i] - s2[i]);
}
