/*
** my_str_isprintable.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:18:17 2014 charti
** Last update Wed Feb 26 16:18:17 2014 charti
*/

#include "my.h"

int	my_str_isprintable(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] < 32 || str[i] > 126)
	return (0);
      ++i;
    }
  return (1);
}
