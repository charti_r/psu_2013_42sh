/*
** my_strlcpy.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:26:28 2014 charti
** Last update Wed Feb 26 16:28:43 2014 charti
*/

#include "my.h"

char	*my_strlcpy(char *dest, char *src, int l)
{
  int	i;

  i = 0;
  while (i < l && src[i])
    {
      dest[i] = src[i];
      ++i;
    }
  return (dest);
}
