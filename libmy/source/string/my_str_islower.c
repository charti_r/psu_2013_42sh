/*
** my_str_islower.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:15:53 2014 charti
** Last update Wed Feb 26 16:15:54 2014 charti
*/

#include "my.h"

int	my_str_islower(char *str)
{
  int   i;

  i = 0;
  while (str[i])
    {
      if (str[i] < 'a' || str[i] > 'z')
	return (0);
      ++i;
    }
  return (1);
}
