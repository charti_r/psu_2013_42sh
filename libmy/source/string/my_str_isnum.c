/*
** my_str_isnum.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:17:47 2014 charti
** Last update Wed Feb 26 16:17:48 2014 charti
*/

#include "my.h"

int     my_str_isnum(char *str)
{
  int   i;

  i = 0;
  while (str[i] == '+' || str[i] == '-')
    ++i;
  while (str[i])
    {
      if (str[i] < '0' || str[i] > '9')
	return (0);
      ++i;
    }
  return (1);
}
