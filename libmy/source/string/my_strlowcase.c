/*
** my_strlowcase.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:32:41 2014 charti
** Last update Wed Feb 26 16:32:42 2014 charti
*/

#include "my.h"

char    *my_strlowcase(char *str)
{
  int   i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= 'A' && str[i] <= 'Z')
	str[i] = str[i] + 32;
      ++i;
    }
  return (str);
}
