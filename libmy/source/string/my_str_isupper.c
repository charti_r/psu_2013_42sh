/*
** my_str_isupper.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:19:07 2014 charti
** Last update Wed Feb 26 16:19:08 2014 charti
*/

#include "my.h"

int     my_str_isupper(char *str)
{
  int   i;

  i = 0;
  while (str[i])
    {
      if (str[i] < 'A' || str[i] > 'Z')
	return (0);
      ++i;
    }
  return (1);
}
