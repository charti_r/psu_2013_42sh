/*
** my_strcpy.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:14:36 2014 charti
** Last update Sun Mar 16 11:02:49 2014 charti
*/

#include "my.h"

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  if (!dest || !src)
    return (NULL);
  i = 0;
  while (src[i])
    {
      dest[i] = src[i];
      ++i;
    }
  dest[i] = '\0';
  return (dest);
}
