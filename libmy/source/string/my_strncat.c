/*
** my_strncat.c for libmy in /home/charti_r/Libraries/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Feb 26 16:33:50 2014 charti
** Last update Wed Feb 26 16:34:21 2014 charti
*/

#include "my.h"

char	*my_strncat(char *dest, char *src, int n)
{
  int	size;
  int	i;

  size = my_strlen(dest);
  i = 0;
  while (i < n && src[i])
    {
      dest[size + i] = src[i];
      ++i;
    }
  dest[size + i] = '\0';
  return (dest);
}
