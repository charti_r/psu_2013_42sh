/*
** my_strcpy_alloc.c for libmy in /home/charti_r/Librarie/libmy/source/string
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Tue Mar 18 23:08:09 2014 charti
** Last update Wed Mar 19 14:24:33 2014 charti
*/

#include "my.h"

char	*my_strcpy_alloc(char *dest, char *src)
{
  int	size;

  if (dest)
    free(dest);
  size = my_strlen(src) + 1;
  if ((dest = malloc(sizeof(*dest) * size)) == NULL)
    return (NULL);
  if ((dest = my_strcpy(dest, src)) == NULL)
    return (NULL);
  return (dest);
}
