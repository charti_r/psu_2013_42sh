/*
** my_putstr.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 12:27:02 2013 charti
** Last update Wed Jan 22 20:28:50 2014 charti
*/

#include "my.h"

int	my_putstr(char *str)
{
  return (write(1, str, my_strlen(str)));
}
