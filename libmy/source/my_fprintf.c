/*
** my_fprintf.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Feb 24 17:40:55 2014 charti
** Last update Tue Feb 25 12:35:57 2014 charti
*/

#include "my.h"

void	my_fprintf_flag(int fd, char flag, va_list ptr_list);

int		my_fprintf(int fd, const char *str, ...)
{
  va_list	ptr_list;
  int		i;

  va_start(ptr_list, str);
  i = 0;
  while (str[i])
    {
      if (str[i] == '%')
	{
	  ++i;
	  my_fprintf_flag(fd, str[i], ptr_list);
	}
      else
	my_fputchar(fd, str[i]);
      ++i;
    }
  va_end(ptr_list);
  return (i);
}

void	my_fprintf_flag(int fd, char flag, va_list ptr_list)
{
  if (flag == 'd' || flag == 'i')
    my_fputnbr(fd, va_arg(ptr_list, int));
  if (flag == 'u')
    my_fputnbr(fd, va_arg(ptr_list, unsigned int));
  if (flag == 'b')
    my_fputnbr_base(fd, va_arg(ptr_list, int), "01");
  if (flag == 'o')
    my_fputnbr_base(fd, va_arg(ptr_list, int), "01234567");
  if (flag == 'p' || flag == 'P')
    my_fputstr(fd, "0x");
  if (flag == 'x' || flag == 'p')
    my_fputnbr_base(fd, va_arg(ptr_list, unsigned int), "0123456789abcdef");
  if (flag == 'X' || flag == 'P')
    my_fputnbr_base(fd, va_arg(ptr_list, unsigned int), "0123456789ABCDEF");
  if (flag == 'c')
    my_fputchar(fd, va_arg(ptr_list, int));
  if (flag == 's')
    my_fputstr(fd, va_arg(ptr_list, char*));
  if (flag == '%')
    my_fputchar(fd, '%');
}
