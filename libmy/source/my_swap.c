/*
** my_swap.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 13:02:46 2013 charti
** Last update Mon Dec 30 13:39:06 2013 charti
*/

#include "my.h"

void	my_swap_int(int *a, int *b)
{
  int	c;

  c = *a;
  *a = *b;
  *b = c;
}

void	my_swap_char(char *a, char *b)
{
  char	c;

  c = *a;
  *a = *b;
  *b = c;
}
