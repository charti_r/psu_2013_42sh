/*
** my_getnbr.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 13:06:25 2013 charti
** Last update Tue Jan 21 14:41:54 2014 charti
*/

#include "my.h"

int	my_getnbr(char *str)
{
  int	i;
  int	nb;

  i = 0;
  nb = 0;
  while (str[i] == '+' || str[i] == '-')
    ++i;
  while (str[i] && str[i] >= '0' && str[i] <= '9')
    {
      nb = nb * 10;
      nb = nb + str[i] - '0';
      ++i;
    }
  return (nb * my_nbr_isneg(str));
}
