/*
** get_next_line.c for libmy in /home/charti_r/Libraries/libmy
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Dec 30 12:03:08 2013 charti
** Last update Mon Jan  6 16:11:56 2014 charti
*/

#include "my.h"

char		*get_next_line(int fd)
{
  static char	*save = NULL;
  char		*buffer;
  int		sz_line;
  int		i;

  buffer = NULL;
  if (save)
    buffer = save;
  sz_line = my_sizeof_line(buffer);
  i = 0;
  while (sz_line == (i * MY_BUFF_SIZE))
    {
      if ((buffer = my_str_realloc(buffer, MY_BUFF_SIZE * (i + 1))) == NULL)
	return (NULL);
      if (read(fd, &buffer[MY_BUFF_SIZE * i], MY_BUFF_SIZE) == -1)
	return (NULL);
      sz_line = my_sizeof_line(buffer);
      i++;
    }
  if (!buffer[0])
    return (NULL);
  if (my_strlen(buffer))
    save = &buffer[sz_line + 1];
  buffer[sz_line] = '\0';
  return (buffer);
}
