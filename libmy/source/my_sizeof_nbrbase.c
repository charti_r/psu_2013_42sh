/*
** my_sizeof_nbrbase.c for libmy in /home/charti_r/Libraries/libmy/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu Feb 27 18:23:26 2014 charti
** Last update Thu Feb 27 18:24:37 2014 charti
*/

#include "my.h"

int	my_sizeof_nbrbase(int nb, int base)
{
  int	len;

  if (!base)
    return (0);
  len = 0;
  while (nb)
    {
      nb /= base;
      ++len;
    }
  return (len);
}
