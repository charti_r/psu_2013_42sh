/*
** parse.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Wed Mar 19 13:27:33 2014 charti
** Last update Sun May 25 15:26:17 2014 dantas_c
*/

#include "mysh.h"

static char	*check_home(char *tmp, char *str, int i, char *value)
{
  if ((tmp = my_strncpy(tmp, str, i - 1)) == NULL)
    return (NULL);
  if ((tmp = my_strcat(tmp, value)) == NULL)
    return (NULL);
  if ((tmp = my_strcat(tmp, "/")) == NULL)
    return (NULL);
  if ((tmp = my_strcat(tmp, &str[i])) == NULL)
    return (NULL);
  return (tmp);
}

static char	*replace_home(t_list *env, char *str)
{
  t_elem	*home;
  char		*tmp;
  int		size;
  int		stop;
  int		i;

  if ((home = get_env_elem(env, "HOME")) == NULL)
    return (str);
  size = my_strlen(str) + my_strlen(home->value) + 1;
  stop = 0;
  i = -1;
  while (str[++i] && !stop)
    if (str[i] == '~')
      if (str[i + 1] == '/' || !str[i + 1])
	{
	  if ((tmp = malloc(sizeof(*tmp) * size)) == NULL)
	    return (NULL);
	  stop = 1;
	}
  if (!stop)
    return (str);
  if ((tmp = check_home(tmp, str, i, home->value)) == NULL)
    return (NULL);
  free(str);
  return (tmp);
}

int		parse_replace(t_list *env, char **argv)
{
  t_elem	*elem;
  int		i;

  i = -1;
  while (argv[++i])
    {
      if (argv[i][0] == '$')
	if ((elem = get_env_elem(env, &argv[i][1])))
	  if ((argv[i] = my_strcpy_alloc(argv[i], elem->value)) == NULL)
	    return (1);
      if ((argv[i] = replace_home(env, argv[i])) == NULL)
	return (1);
    }
  return (0);
}
