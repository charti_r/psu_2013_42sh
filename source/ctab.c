/*
** ctab.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu May 15 11:52:07 2014 charti
** Last update Fri May 30 14:37:56 2014 charti
*/

#include "mysh.h"

void	my_aff_ctab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i])
    {
      if (i)
	my_putchar(' ');
      my_putstr(tab[i]);
      ++i;
    }
}

int	my_count_double_ctab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i])
    ++i;
  return (i);
}

void	my_free_double_ctab(char **tab)
{
  int	i;

  i = -1;
  while (tab[++i])
    free(tab[i]);
  free(tab);
}
