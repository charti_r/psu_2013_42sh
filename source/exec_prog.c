/*
** exec_prog.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Fri May 16 10:32:19 2014 charti
** Last update Sun Jun  1 16:31:10 2014 charti
*/

#include "mysh.h"

static char	**my_get_argv(char *cmd, char *sep, t_list *env)
{
  char		**argv;

  if ((argv = my_str_to_wordtab(cmd, sep)) == NULL)
    return (NULL);
  if (parse_replace(env, argv))
    return (NULL);
  return (argv);
}

static int	run_parent(t_pipe *p)
{
  close(p->p[1]);
  p->fd_s = p->p[0];
  return (parent());
}

static void	run_child(char **argv, t_list *env, t_pipe *p, int last)
{
  char		**cmd;
  int		fd;
  int		pos;

  close(p->p[0]);
  if ((last & LST) && dup2(p->fd_s, 0) == -1)
    exit(1);
  if ((last & FST) && dup2(p->p[1], 1) == -1)
    exit(1);
  fd = -1;
  if (redir(argv, &fd) == -1)
    exit(1);
  if ((cmd = clean_argv(argv)) == NULL)
    exit(1);
  if (cmd[0] == NULL)
    exit(0);
  if (out_own_cmd(cmd, env) == 2)
    child(cmd, env);
  if (fd != -1)
    close(fd);
  free(cmd);
  exit(0);
}

static int	run_prog(char **cmd, t_list *env)
{
  char		**argv;
  int		ret;
  pid_t		pid;
  t_pipe	p;
  int		i;

  i = -1;
  while (cmd[++i])
    {
      if (pipe(p.p) == -1 ||
	  (argv = my_get_argv(cmd[i], ARG_SEP, env)) == NULL)
	return (-1);
      if ((ret = in_own_cmd(my_count_double_ctab(argv), argv, env)) != 2)
	return (ret);
      if ((pid = fork()) == -1)
	return (-2);
      if (!pid)
	run_child(argv, env, &p, PIPE(i, cmd[i + 1]));
      my_free_double_ctab(argv);
      if (run_parent(&p))
	return (1);
    }
  return (0);
}

int	exec_prog(char *str, t_list *env)
{
  char	**cmd;
  int	ret;

  if ((cmd = my_str_to_wordtab(str, PIPE_SEP)) == NULL)
    return (-1);
  if (my_strlen(cmd[0]) == 0)
    {
      my_fputstr(2, SYNTAX_ERROR);
      return (1);
    }
  if ((ret = run_prog(cmd, env)))
    {
      if (ret == -2)
	my_fputstr(2, FAILED_FORK);
      my_free_double_ctab(cmd);
      return (ret == -2 ? -1 : ret);
    }
  my_free_double_ctab(cmd);
  return (0);
}
