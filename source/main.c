/*
** main.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Mar 15 12:06:24 2014 charti
** Last update Sun May 25 18:57:51 2014 charti
*/

#include "mysh.h"

static void	init_sig(t_list *env)
{
  sigint_catch(0, env);
  signal(SIGINT, (void (*)(int))(&sigint_catch));
}

int		main(int argc, char **argv, char **env)
{
  t_list	list;
  char		*buffer;
  int		add;

  (void)argc;
  (void)argv;
  if (cp_env(&list, env))
    return (1);
  init_sig(&list);
  add = 0;
  while (1)
    {
      prompt(&list, add);
      if ((buffer = read_line()) == NULL)
	return (my_putstr(USER_EXIT));
      my_replace_char(buffer, '\t', ' ');
      if ((add = my_strlen(buffer) ? 1 : 0) && (exec_cmd(buffer, &list)) == -1)
	return (my_putchar('\n'));
      free(buffer);
    }
  return (0);
}
