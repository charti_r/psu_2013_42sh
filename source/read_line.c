/*
** read_line.c for rush2 in /home/charti_r/Programming/rush2
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat May 10 11:16:22 2014 charti
** Last update Wed May 21 12:02:56 2014 charti
*/

#include "mysh.h"

char	*read_line(void)
{
  char	*line;
  char	buffer[MY_BUFF_SIZE];
  int	ret;
  int	count;

  count = 0;
  if ((line = malloc(sizeof(*line))) == NULL)
    return (NULL);
  line[0] = 0;
  while ((ret = read(0, buffer, MY_BUFF_SIZE)) > 0)
    {
      buffer[ret] = '\0';
      if ((line = my_str_realloc(line, (count * MY_BUFF_SIZE) + ret)) == NULL)
	return (NULL);
      my_strcat(line, buffer);
      if (buffer[ret - 1] == '\n')
	{
	  line[(count * MY_BUFF_SIZE) + (ret - 1)] = '\0';
	  return (line);
	}
      if (buffer[ret - 1])
	++count;
    }
  return (NULL);
}
