/*
** echo_cmd.c for 42sh in /home/dantas_c/42sh/psu_2013_42sh/source
** 
** Made by dantas_c
** Login   <dantas_c@epitech.net>
** 
** Started on  Fri May 16 14:32:18 2014 dantas_c
** Last update Sun May 25 15:48:00 2014 dantas_c
*/

#include "mysh.h"

static void	echo_e(char **argv)
{
  int		i;
  int		j;

  j = 0;
  i = 0;
  while (argv[i])
    {
      if (i)
	my_putchar(' ');
      while (argv[i][j])
	{
	  if (argv[i][j] == 92)
	    ++j;
	  my_putchar(argv[i][j]);
	  ++j;
	}
      j = 0;
      ++i;
    }
  return ;
}

int	echo_cmd(char **argv)
{
  if (argv[1])
    {
      if (!my_strcmp(argv[1], "-n"))
	{
	  if (!my_strcmp(argv[2], "-e"))
	    echo_e(&argv[3]);
	  else
	    echo_e(&argv[2]);
	  return (0);
	}
      else if (!my_strcmp(argv[1], "-e"))
	{
	  if (!my_strcmp(argv[2], "-n"))
	    {
	      echo_e(&argv[3]);
	      return (0);
	    }
	  else
	    echo_e(&argv[2]);
	}
      else
	echo_e(&argv[1]);
    }
  my_putchar('\n');
  return (0);
}
