/*
** exit_cmd.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source/command
** 
** Made by charti
** Login   <@epitech.net>
** 
** Started on  Mon Mar 17 23:46:54 2014 charti
** Last update Mon Mar 17 23:47:01 2014 charti
*/

#include "mysh.h"

void	exit_shell(char **argv)
{
  if (argv[1])
    exit(my_getnbr(argv[1]));
  exit(0);
}
