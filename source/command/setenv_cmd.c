/*
** setenv_cmd.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source/command
** 
** Made by charti
** Login   <@epitech.net>
** 
** Started on  Mon Mar 17 23:48:20 2014 charti
** Last update Sun May 25 15:39:56 2014 charti
*/

#include "mysh.h"

int	setenv_cmd(int argc, char **argv, t_list *env)
{
  if (argc == 3)
    return (set_env_line(argv[1], argv[2], env));
  if (argc == 2)
    return (set_env_line(argv[1], "", env));
  my_fputstr(2, SETENV_FAILED);
  return (1);
}
