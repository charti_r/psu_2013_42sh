/*
** unsetenv_cmd.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source/command
** 
** Made by charti
** Login   <@epitech.net>
** 
** Started on  Mon Mar 17 23:48:45 2014 charti
** Last update Thu May  8 22:15:48 2014 abboud_j
*/

#include "mysh.h"

int	unsetenv_cmd(char **argv, t_list *env)
{
  if (argv[1])
    return (del_env_line(my_strupcase(argv[1]), env));
  my_fputstr(2, UNSETENV_FAILED);
  return (0);
}
