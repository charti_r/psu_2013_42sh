/*
** env_cmd.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source/command
** 
** Made by charti
** Login   <@epitech.net>
** 
** Started on  Mon Mar 17 23:47:50 2014 charti
** Last update Wed May 14 17:15:50 2014 charti
*/

#include "mysh.h"

int		env_cmd(t_list *env)
{
  t_elem	*elem;

  elem = env->first;
  while (elem)
    {
      my_printf("%s=%s\n", elem->var, elem->value);
      elem = elem->next;
    }
  return (0);
}
