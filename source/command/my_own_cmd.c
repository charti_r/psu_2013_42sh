/*
** my_own_cmd.c for  in /home/charti_r/rendu/PSU_2013_42sh/source/command
** 
** Made by charti
** Login   <@epitech.net>
** 
** Started on  Mon Mar 17 23:44:02 2014 charti
** Last update Fri May 30 14:46:23 2014 charti
*/

#include "mysh.h"

int	in_own_cmd(int argc, char **argv, t_list *env)
{
  int	ret;

  ret = 2;
  if (!my_strcmp(argv[0], CMD_EXIT))
    exit_shell(argv);
  if (!my_strcmp(argv[0], CMD_CD))
    ret = cd_cmd(argv, env);
  else if (!my_strcmp(argv[0], CMD_SETENV))
    ret = setenv_cmd(argc, argv, env);
  else if (!my_strcmp(argv[0], CMD_UNSETENV))
    ret = unsetenv_cmd(argv, env);
  if (ret != 2)
    my_free_double_ctab(argv);
  return (ret);
}

int	out_own_cmd(char **argv, t_list *env)
{
  if (!my_strcmp(argv[0], CMD_ENV))
    return (env_cmd(env));
  else if (!my_strcmp(argv[0], CMD_ECHO))
    return (echo_cmd(argv));
  else if (my_strstr(argv[0], "kill") != -1)
    setpgid(getpid(), getpgid(getpid()) + 1);
  return (2);
}
