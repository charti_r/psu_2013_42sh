/*
** cd_pwd_change.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source/command
** 
** Made by charti
** Login   <@epitech.net>
** 
** Started on  Tue Mar 18 12:20:29 2014 charti
** Last update Sun May 25 17:34:29 2014 charti
*/

#include "mysh.h"

static char	*cd_pwd_backto_root(char *str)
{
  free(str);
  if ((str = malloc(sizeof(*str) * 2)) == NULL)
    return (NULL);
  if ((str = my_strcpy(str, "/")) == NULL)
    return (NULL);
  return (str);
}

static char	*cd_pwd_backward(char *str)
{
  char		*tmp;
  int		i;

  i = my_strlen(str) - 1;
  while (i && str[i] != '/')
    --i;
  i = (!i ? 1 : i);
  if ((tmp = malloc(sizeof(*tmp) * (i + 1))) == NULL)
    return (NULL);
  if ((tmp = my_strncpy(tmp, str, i)) == NULL)
    return (NULL);
  free(str);
  return (tmp);
}

static char	*cd_pwd_enterdir(char *str, char *path, int nb)
{
  char		*tmp;
  int		slash;
  int		size;

  size = my_strlen(str) + my_strlen(path);
  slash = ((str[size - 1 - my_strlen(path)] == '/') ? 0 : 1);
  size += nb + slash;
  if ((tmp = malloc(sizeof(*tmp) * size + 1)) == NULL)
    return (NULL);
  if ((tmp = my_strcpy(tmp, str)) == NULL)
    return (NULL);
  if (slash)
    if ((tmp = my_strcat(tmp, "/")) == NULL)
      return (NULL);
  free(str);
  return (my_strcat(tmp, path));
}

static int	end_cd_pwd(t_elem *pwd, char *tmp)
{
  pwd->value = my_strcpy_alloc(pwd->value, tmp);
  free(tmp);
  return (0);
}

int	cd_pwd_change(char *str, t_elem *pwd, t_elem *oldpwd)
{
  int	i;
  int	j;
  char	cmp;
  char	*tmp;

  if ((tmp = my_strcpy_alloc(NULL, pwd->value)) == NULL ||
      (oldpwd->value = my_strcpy_alloc(oldpwd->value, pwd->value)) == NULL)
    return (0);
  i = 0;
  while (str[i])
    {
      j = 0;
      while (str[i + j] != '/' && str[i + j])
	++j;
      if (!j)
	tmp = cd_pwd_backto_root(tmp);
      else if (!(cmp = my_strncmp(&str[i], "..", j)) || cmp == '/')
	tmp = cd_pwd_backward(tmp);
      else
	tmp = cd_pwd_enterdir(tmp, &str[i], j);
      if (!tmp)
	return (1);
      i += (str[i + j] ? j + 1 : j);
    }
  return (end_cd_pwd(pwd, tmp));
}
