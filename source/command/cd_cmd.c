/*
** cd_cmd.c for  in /home/charti_r/rendu/PSU_2013_42sh/source/command
** 
** Made by charti
** Login   <@epitech.net>
** 
** Started on  Mon Mar 17 23:45:44 2014 charti
** Last update Thu May 29 07:48:33 2014 chauvi_r
*/

#include "mysh.h"

/*
** Set the old pwd
*/
static int	cd_pwd_old(t_elem *pwd, t_elem *oldpwd)
{
  char		*tmp;

  tmp = pwd->value;
  pwd->value = oldpwd->value;
  oldpwd->value = tmp;
  return (0);
}

static int	cd_pwd_home(t_elem *pwd, t_elem *oldpwd, t_elem *home)
{
  int		size;

  if (!home)
    return (0);
  size = my_strlen(pwd->value) + 1;
  free(oldpwd->value);
  if ((oldpwd->value = malloc(sizeof(*(oldpwd->value)) * size)) == NULL)
    return (0);
  if ((oldpwd->value = my_strcpy(oldpwd->value, pwd->value)) == NULL)
    return (0);
  size = my_strlen(home->value) + 1;
  free(pwd->value);
  if ((pwd->value = malloc(sizeof(*(pwd->value)) * size)) == NULL)
    return (0);
  if ((pwd->value = my_strcpy(pwd->value, home->value)) == NULL)
    return (0);
  return (0);
}

static int	cd_pwd(char *str, t_list *env)
{
  t_elem	*pwd;
  t_elem	*oldpwd;

  if ((pwd = get_env_elem(env, "PWD")) == NULL)
    {
      if (add_elem(env, "PWD", ""))
	return (0);
      pwd = env->first;
    }
  if ((oldpwd = get_env_elem(env, "OLDPWD")) == NULL)
    {
      if (add_elem(env, "OLDPWD", ""))
	return (0);
      oldpwd = env->first;
    }
  if (!str)
    return (cd_pwd_home(pwd, oldpwd, get_env_elem(env, "HOME")));
  if (!(my_strcmp(str, "-")))
    return (cd_pwd_old(pwd, oldpwd));
  return (cd_pwd_change(str, pwd, oldpwd));
}

static int	check_cd_cmd(char **argv)
{
  if (!my_strcmp(argv[1], "."))
    return (0);
  if (!my_strcmp(argv[1], "-"))
    return (1);
  if (!(access(argv[1], F_OK)))
    {
      if (access(argv[1], R_OK))
	{
	  my_fprintf(2, CD_DENIED, argv[1]);
	  return (-1);
	}
    }
  else
    {
      my_fprintf(2, CD_NOT_FOUND, argv[1]);
      return (-1);
    }
  return (1);
}

int	cd_cmd(char **argv, t_list *env)
{
  int	ret;

  ret = 0;
  if (argv[1])
    {
      if ((ret = check_cd_cmd(argv)) != 1)
	return (1);
      if (!my_strcmp(argv[1], "-"))
	{
	  if ((ret = chdir(get_env_line("OLDPWD", env))) != -1)
	    my_printf("%s\n", get_env_line("OLDPWD", env));
	  else
	    my_fprintf(2, "Error : variable OLDPWD is empty. Retry\n");
	  ret = 1;
	}
      else if ((ret = chdir(argv[1])) == -1)
	return (-1);
    }
  else
    ret = chdir(get_env_line("HOME", env));
  if (!ret)
    return (cd_pwd(argv[1], env));
  return (ret);
}
