/*
** sig_catch.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sun Mar 16 22:45:34 2014 charti
** Last update Sun May 25 18:57:29 2014 charti
*/

#include "mysh.h"

void		sigint_catch(int sig, t_list *ptr_env)
{
  static t_list	*env = NULL;

  signal(sig, (void (*)(int))(&sigint_catch));
  if (!env)
    env = ptr_env;
  else
    {
      my_putchar('\n');
      prompt(env, 0);
    }
}
