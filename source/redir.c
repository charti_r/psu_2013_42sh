/*
** redir.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu May 15 13:07:30 2014 charti
** Last update Fri May 30 14:38:06 2014 charti
*/

#include "mysh.h"

static int	redir_sread(char *file, int *fd)
{
  if (*fd != -1)
    close(*fd);
  if (dup2((*fd = open(file, RD_FILE, MODE)), 0) == -1)
    {
      my_printf(CD_NOT_FOUND, file);
      return (-1);
    }
  return (0);
}

static int	redir_swrite(char *file, int *fd)
{
  if (*fd != -1)
    close(*fd);
  if (dup2((*fd = open(file, WR_FILE | O_TRUNC, MODE)), 1) == -1)
    return (-1);
  return (0);
}

static int	redir_dread(char *eof, int *fd)
{
  int		ret;
  char		buff[MY_BUFF_SIZE];

  if (*fd != -1)
    close(*fd);
  if ((*fd = open(".tmp", WR_FILE | O_TRUNC, MODE)) == -1)
    return (-1);
  while ((ret = read(0, buff, MY_BUFF_SIZE)) > 0)
    {
      buff[ret] = '\0';
      if (!my_strncmp(buff, eof, my_strlen(eof) - 1))
	{
	  close(*fd);
	  if (dup2((*fd = open(".tmp", RD_FILE, MODE)), 0) == -1)
	    return (-1);
	  return (0);
	}
      my_fputstr(*fd, buff);
    }
  return (0);
}

static int	redir_dwrite(char *file, int *fd)
{
  if (*fd != -1)
    close(*fd);
  if (dup2((*fd = open(file, WR_FILE | O_APPEND, MODE)), 1) == -1)
    return (-1);
  return (0);
}

int	redir(char **cmd, int *fd)
{
  int	i;

  i = 0;
  while (cmd[i])
    {
      if (!my_strcmp(cmd[i], "<") && cmd[i + 1] &&
	  redir_sread(cmd[i + 1], fd) == -1)
	return (-1);
      else if (!my_strcmp(cmd[i], ">") && cmd[i + 1] &&
	       redir_swrite(cmd[i + 1], fd) == -1)
	return (-1);
      else if (!my_strcmp(cmd[i], "<<") && cmd[i + 1] &&
	       redir_dread(cmd[i + 1], fd) == -1)
	return (-1);
      else if (!my_strcmp(cmd[i], ">>") && cmd[i + 1] &&
	       redir_dwrite(cmd[i + 1], fd) == -1)
	return (-1);
      ++i;
    }
  return (0);
}
