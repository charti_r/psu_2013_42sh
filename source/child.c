/*
** child.c for child in /home/chauvi_r/rendu/psu_2013_42sh
** 
** Made by chauvi_r
** Login   <chauvi_r@epitech.net>
** 
** Started on  Sat May 24 17:02:48 2014 chauvi_r
** Last update Fri May 30 14:49:11 2014 charti
*/

#include "mysh.h"

static char	*check_path(char *path, char *full_path, char **argv)
{
  char		**tab_path;
  int		i;

  i = 0;
  if ((tab_path = my_str_to_wordtab(full_path, ":")) == NULL)
    return (NULL);
  while (tab_path[i] != NULL)
    {
      if ((path = malloc(sizeof(*path) *
			 (my_strlen(tab_path[i])
			  + my_strlen(argv[0]) + 2))) == NULL)
	return (NULL);
      my_strcpy(path, tab_path[i]);
      path = my_strcat(path, "/");
      path = my_strcat(path, argv[0]);
      if (access(path, X_OK) == 0)
	{
	  my_free_double_ctab(tab_path);
	  return (path);
	}
      free(path);
      ++i;
    }
  my_free_double_ctab(tab_path);
  return (NULL);
}

int	child(char **argv, t_list *env)
{
  char	**env_tab;
  char	*full_path;
  char	*path;

  path = NULL;
  if ((env_tab = env_from_list(env)) == NULL)
    exit(1);
  execve(argv[0], argv, env_tab);
  if ((full_path = get_env_line("PATH", env)) == NULL)
    {
      my_fprintf(2, CMD_NOT_FOUND, argv[0]);
      my_free_double_ctab(env_tab);
      exit(1);
    }
  path = check_path(path, full_path, argv);
  execve(path, argv, env_tab);
  my_fprintf(2, CMD_NOT_FOUND, argv[0]);
  my_free_double_ctab(env_tab);
  if (path)
    free(path);
  exit(1);
}
