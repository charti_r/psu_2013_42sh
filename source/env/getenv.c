/*
** getenv.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source/env
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Mar 15 20:12:23 2014 charti
** Last update Sat Mar 15 20:12:23 2014 charti
*/

#include "mysh.h"

char		*get_env_line(char *var, t_list *env)
{
  t_elem	*elem;
  int		stop;

  elem = env->first;
  stop = 0;
  while (elem && !stop)
    if (my_strcmp(elem->var, var))
      elem = elem->next;
    else
      stop = 1;
  if (!elem)
    return (NULL);
  return (elem->value);
}
