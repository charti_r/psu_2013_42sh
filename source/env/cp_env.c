/*
** cp_env.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source/env
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Mar 15 20:12:15 2014 charti
** Last update Wed May 14 16:50:48 2014 charti
*/

#include "mysh.h"

static char	*get_var_value(char *env)
{
  char		*cpy;
  int		i;

  i = 0;
  while (env[i] != '=')
    ++i;
  ++i;
  if ((cpy = malloc(sizeof(*cpy) * (my_strlen(&env[i]) + 1))) == NULL)
    return (NULL);
  return (my_strcpy(cpy, &env[i]));

}

static char	*get_var_name(char *env)
{
  char		*cpy;
  int		i;

  i = 0;
  while (env[i] != '=')
    ++i;
  if ((cpy = malloc(sizeof(*cpy) * (i + 1))) == NULL)
    return (NULL);
  return (my_strncpy(cpy, env, i));
}

int	cp_env(t_list *list, char **env)
{
  char	*var;
  char	*value;
  int	i;

  init_list(list);
  i = -1;
  while (env[++i])
    {
      if ((var = get_var_name(env[i])) == NULL)
	return (1);
      if ((value = get_var_value(env[i])) == NULL)
	return (1);
      if (add_elem(list, var, value))
	return (1);
      free(var);
      free(value);
    }
  return (0);
}
