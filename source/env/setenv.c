/*
** setenv.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source/env
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Mar 15 20:12:38 2014 charti
** Last update Mon Mar 17 16:27:17 2014 charti
*/

#include "mysh.h"

int		set_env_line(char *var, char *value, t_list *env)
{
  t_elem	*elem;
  int		stop;

  elem = env->first;
  stop = 0;
  while (elem && !stop)
    {
      if (my_strcmp(elem->var, var))
	elem = elem->next;
      else
	stop = 1;
    }
  if (!elem)
    return (add_elem(env, var, value));
  free(elem->value);
  if ((elem->value = malloc(sizeof(*value) * (my_strlen(value) + 1))) == NULL)
    return (1);
  if ((elem->value = my_strcpy(elem->value, value)) == NULL)
    return (1);
  return (0);
}
