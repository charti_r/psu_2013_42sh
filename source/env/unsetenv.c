/*
** unsetenv.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source/env
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Mar 15 20:12:44 2014 charti
** Last update Thu May  8 22:11:49 2014 abboud_j
*/

#include "mysh.h"

int	del_env_line(char *var, t_list *env)
{
  if (del_elem(env, var))
    my_fprintf(2, VAR_NOT_FOUND, var);
  return (0);
}
