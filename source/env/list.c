/*
** list.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source/env
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Mar 15 20:12:31 2014 charti
** Last update Sun May 25 16:41:26 2014 charti
*/

#include "mysh.h"

/*
** Initialise the list
*/
void	init_list(t_list *list)
{
  list->first = NULL;
  list->nb = 0;
}

/*
** Add a new elem with its var and its value.
*/
int		add_elem(t_list *list, char *var, char *value)
{
  t_elem	*elem;

  if (!list)
    return (1);
  ++(list->nb);
  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (1);
  if ((elem->var = malloc(sizeof(*var) * (my_strlen(var) + 1))) == NULL)
    return (1);
  if ((elem->var = my_strcpy(elem->var, var)) == NULL)
    return (1);
  if ((elem->value = malloc(sizeof(*value) * (my_strlen(value) + 1))) == NULL)
    return (1);
  if ((elem->value = my_strcpy(elem->value, value)) == NULL)
    return (1);
  elem->next = list->first;
  list->first = elem;
  return (0);
}

/*
** Delete the elem named by its var form the list.
*/
int		del_elem(t_list *list, char *var)
{
  t_elem	*elem;
  t_elem	*prev;
  int		stop;

  elem = list->first;
  prev = NULL;
  while (elem && (stop = my_strcmp(elem->var, var)))
    {
      prev = elem;
      elem = elem->next;
    }
  if (!elem)
    return (1);
  --(list->nb);
  free(elem->var);
  free(elem->value);
  if (prev)
    prev->next = elem->next;
  else
    list->first = elem->next;
  free(elem);
  return (0);
}

/*
** Return the env wich is a tab from list.
*/
char		**env_from_list(t_list *list)
{
  char		**env;
  t_elem	*elem;
  int		size;
  int		i;

  elem = list->first;
  if ((env = malloc(sizeof(*env) * (list->nb + 1))) == NULL)
    return (NULL);
  i = list->nb;
  env[i] = NULL;
  while (--i >= 0)
    {
      size = my_strlen(elem->var) + my_strlen(elem->value) + 2;
      if ((env[i] = malloc(sizeof(**env) * size)) == NULL)
	return (NULL);
      env[i] = my_strcpy(env[i], elem->var);
      env[i] = my_strcat(env[i], "=");
      env[i] = my_strcat(env[i], elem->value);
      elem = elem->next;
    }
  return (env);
}

/*
** Returns a pointer on the right elem var from env list.
*/
t_elem		*get_env_elem(t_list *list, char *var)
{
  t_elem	*elem;
  int		stop;

  elem = list->first;
  stop = 0;
  while (elem && !stop)
    {
      if (my_strcmp(elem->var, var))
	elem = elem->next;
      else
	stop = 1;
    }
  return (elem);
}
