/*
** parent.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Sat Mar 15 12:06:46 2014 charti
** Last update Sun May 25 18:09:53 2014 charti
*/

#include "mysh.h"

int	parent(void)
{
  int	stat;

  kill(wait(&stat), SIGKILL);
  if (stat == SIGQUIT)
    my_fputstr(2, SIG_QUIT);
  else if (stat == SIGILL)
    my_fputstr(2, SIG_ILLEGAL);
  else if (stat == SIGABRT)
    my_fputstr(2, SIG_ABORT);
  else if (stat == SIGFPE)
    my_fputstr(2, SIG_FLOAT);
  else if (stat == SIGSEGV)
    my_fputstr(2, SIG_SEGV);
  return (stat >> 8);
}
