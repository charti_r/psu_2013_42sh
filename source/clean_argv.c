/*
** clean_argv.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu May 29 16:21:03 2014 charti
** Last update Fri May 30 14:37:46 2014 charti
*/

#include "mysh.h"

static int	get_size_clean_argv(char **argv)
{
  int		size;
  int		i;

  size = 0;
  i = 0;
  while (argv[i])
    {
      if ((!my_strcmp(argv[i], "<") || !my_strcmp(argv[i], ">") ||
	   !my_strcmp(argv[i], "<<") || !my_strcmp(argv[i], ">>"))
	  && argv[i + 1])
	++i;
      else
	++size;
      ++i;
    }
  return (size);
}

char	**clean_argv(char **argv)
{
  char	**cmd;
  int	size;
  int	i;
  int	j;

  size = get_size_clean_argv(argv);
  if ((cmd = malloc(sizeof(*cmd) * (size + 1))) == NULL)
    return (NULL);
  i = 0;
  j = 0;
  while (i < size)
    {
      if ((!my_strcmp(argv[j], "<") || !my_strcmp(argv[j], ">") ||
	   !my_strcmp(argv[j], "<<") || !my_strcmp(argv[j], ">>"))
	  && argv[++j])
	++j;
      cmd[i] = argv[j];
      ++i;
      ++j;
    }
  cmd[i] = NULL;
  return (cmd);
}
