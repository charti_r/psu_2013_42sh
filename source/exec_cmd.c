/*
** exec_cmd.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Thu May 15 11:36:33 2014 charti
** Last update Sun May 25 18:02:43 2014 charti
*/

#include "mysh.h"

static int	exec_cmd_and(char *cmd, t_list *env)
{
  char		**cmd_and;
  int		ret;
  int		stop;
  int		i;

  if ((cmd_and = my_str_to_wordtab(cmd, CMD_AND_SEP)) == NULL)
    return (-1);
  stop = 0;
  i = 0;
  while (cmd_and[i] && !stop)
    {
      if ((ret = exec_prog(cmd_and[i], env)))
	stop = 1;
      ++i;
    }
  my_free_double_ctab(cmd_and);
  return (ret);
}

static int	exec_cmd_or(char *cmd, t_list *env)
{
  char		**cmd_or;
  int		ret;
  int		stop;
  int		i;

  if ((cmd_or = my_str_to_wordtab(cmd, CMD_OR_SEP)) == NULL)
    return (-1);
  stop = 0;
  i = 0;
  while (cmd_or[i] && !stop)
    {
      if ((ret = exec_cmd_and(cmd_or[i], env)) == -1)
	{
	  my_free_double_ctab(cmd_or);
	  return (-1);
	}
      stop = ret ? 0 : 1;
      ++i;
    }
  my_free_double_ctab(cmd_or);
  return (0);
}

int	exec_cmd(char *str, t_list *env)
{
  char	**cmd;
  int	i;

  if ((cmd = my_str_to_wordtab(str, CMD_SEP)) == NULL)
    return (-1);
  i = 0;
  while (cmd[i])
    {
      if (exec_cmd_or(cmd[i], env) == -1)
	{
	  my_free_double_ctab(cmd);
	  return (-1);
	}
      ++i;
    }
  my_free_double_ctab(cmd);
  return (0);
}
