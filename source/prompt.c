/*
** prompt.c for 42sh in /home/charti_r/rendu/PSU_2013_42sh/source
** 
** Made by charti
** Login   <charti_r@epitech.net>
** 
** Started on  Mon Mar 17 17:13:31 2014 charti
** Last update Sun May 25 16:34:20 2014 chauvi_r
*/

#include "mysh.h"

static char	*get_prompt_value(t_list *env, char var, int count)
{
  t_elem	*elem;

  if (var == 'u')
    {
      if ((elem = get_env_elem(env, "USER")) == NULL)
	elem = get_env_elem(env, "LOGNAME");
      if (elem)
	return (elem->value);
    }
  if (var == 'h')
    {
      if ((elem = get_env_elem(env, "HOST")) == NULL)
	elem = get_env_elem(env, "HOSTNAME");
      if (elem)
	return (elem->value);
    }
  if (var == 'w')
    if ((elem = get_env_elem(env, "PWD")))
      return (elem->value);
  if (var == '#' || var == '!')
    my_putnbr(count);
  return ("");
}

void		prompt(t_list *env, int add)
{
  static int	count = 1;
  t_elem	*prompt;
  int		i;

  count = add ? count + 1 : count;
  if ((prompt = get_env_elem(env, "PS1")) != NULL
      || (prompt = get_env_elem(env, "USER")) != NULL
      || (prompt = get_env_elem(env, "LOGNAME")) != NULL)
    {
      i = -1;
      while (prompt->value[++i])
	if (prompt->value[i] == '\\' && prompt->value[i + 1])
	  my_putstr(get_prompt_value(env, prompt->value[++i], count));
	else
	  my_putchar(prompt->value[i]);
    }
  if (get_env_elem(env, "PS1") == NULL)
    my_putstr(DEFAULT_PROMPT);
}
